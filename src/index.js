import to from './to';
import { default as from } from './from';
export { get, set, parts, unflatten, keyMapper } from './utils';
const interpolate = from;
const unterpolate = to;

export { to, from, interpolate, unterpolate };
